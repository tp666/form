Vue.component("tp",{
	props:{
		data:{
            required:true
        }
	},
	data(){
        return{
		   cols:0,
		   rows:0
		}
	},
	mounted() {
		this.cols=this.data.cols
		this.rows=this.data.rows
        for (let i = 0; i < this.cols.length; i++) {
			this.cols[i].type="normal"
        }
        this.data.cols=this.cols

	},
	methods:{
        order(index,t,key){
        	this.cols.forEach((col,i)=>{
                if(index!==i){
                    col.type="normal"
                }else{
                    col.type=t
                    this.$set(this.cols,index,col)
                }
			})
            this.rows.sort((a,b)=>{
            	if(t==='asc'){
                    return a[key]>b[key]? 1: -1
				}else{
                    return a[key]>b[key]? -1: 1
				}
			})

        },

},
	template:`
	   <table class="a">
		   <tr>
				<th v-for="(item,index) in cols"> {{item.title}}
				<span v-if="item.sortable">
				<label @click="order(index,'asc',item.key)" :class="{active:item.type==='asc'}"  class="pai">↑</label>
				<label @click="order(index,'desc',item.key)" :class="{active:item.type==='desc'}" class="pai">↓</label>
               </span>
				</th> 
				
		   </tr> 
		   <tr v-for="item in rows">
				<th> {{item.id}} </th> 
				<th> {{item.name}} </th> 
				<th> {{item.age}} </th>
				<th> {{item.scroe}} </th>  
		   </tr> 
	   </table>
	`
})
